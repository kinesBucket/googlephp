<?php
// https://cloud.google.com/datastore/docs/reference/libraries
// https://cloud.google.com/datastore/docs/concepts/queries#datastore-datastore-ascending-sort-php
// https://googleapis.github.io/google-cloud-php/#/docs/google-cloud/v0.97.0/datastore/datastoreclient

// require_once __DIR__ . '../vendor/autoload.php';
require __DIR__ . '/vendor/autoload.php';

use Google\Cloud\Datastore\DatastoreClient;
use Google\Cloud\Datastore\Entity;

$projectId = 'focal-equinox-120122';
$datasetId = $projectId;
$datastore = new DatastoreClient([
    'projectId' => $projectId,
    'keyFile' => json_decode(file_get_contents('OOMDO-a3e0258d7644.json'), true),
    'keyFilePath' => 'OOMDO-a3e0258d7644.json'
]);

$query = $datastore->gqlQuery('SELECT * FROM Special WHERE client_id = @client_id  and active = @active and expiration > @today ', [
    'bindings' => [
        'client_id' => '12',
        'today' => 1555012191266 ,
        'active' => 'true'
    ]
]);


$results = $datastore->runQuery($query);

$specials = [];
$nextPageCursor = null;
foreach ($results as $entity) {
    $special = $entity->get();
    $special['id'] = $entity->key()->pathEndIdentifier();
    $specials[] = $special;
    echo "client name = ".$special['client_name']."<br>";
    // echo "expire ". date("d-m-Y", $special['expiration']/1000)."<br>";
    echo "expire = ". $special['expiration']."<br>";
    echo "img url = ". $special['special_details']['image_url'] ."<br>";

    $vehicle = $special['special_details']['year']." ".$special['special_details']['make']." ".
    $special['special_details']['model']." ".$special['special_details']['trim'];
    echo "Vehicle = ". $vehicle ."<br>";
    echo "disclaimer = ". $special['special_details']['disclaimer'] ."<br>";
    // var_dump($special['special_details']);
    // {"abbreviated_disclaimer_other":"","body_type":"4D Hatchback","disclaimer":"2019 Honda Fit LX $17,085 MSRP. Model Code GK5G4KEW. VIN 3HGGK5G46KM707216. Stock#H190182. 4 cylinder engine, 6-Speed Manual transmission, 4D Hatchback.  *APR finance offer 1.9% for 36 months with approved credit through Honda Financial Services with tier 1 approval (720+ beacon score). $28.60 per month, per $1,000 financed. Not all will qualify. Must take delivery from retail stock. Prior sales excluded. Offer subject to change. Offer expires on 02/28/2019.","vin":"3HGGK5G46KM707216","Other_Discounts":{"Other3":"false","Other2":"false"},"current_selected":["APR"],"disclaimer_intro":"","offer_number":"single","expiration":"02/28/2019","abbreviated_disclaimer_lease":"","discounts":{"Conquest":{"amount":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false"},"Other2":{"amount":"","name":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false"},"Other":{"Discount_From_MSRP":"false","APR":"false","amount":"","name":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false"},"Military":{"Discount_From_MSRP":"false","APR":"false","amount":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false"},"Loyalty":{"amount":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false"},"Other3":{"Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false","amount":"","name":""},"College_Grad":{"amount":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false"},"Dealer":{"amount":"","Finance_Payment":"false","status":"false","Lease":"false","Low_Sale_Price":"false","Discount_From_MSRP":"false","APR":"false"}},"model_number":"GK5G4KEW","max_selected":"1","msrp":"17085","cylinders":"4","model":"Fit","disclaimer_specs":{"APR":{"financing_lender":"Honda Financial Services","credit_qualification":"tier 1 approval (720+ beacon score)","down_payment":{"percentage":"","status":"false"}},"Finance_Payment":{"financing_lender":"Honda Financial Services","credit_qualification":"tier 1 approval (720+ beacon score)","down_payment":{"status":"false","percentage":""}},"Lease":{"acquisition_fee":"","disposition_fee":"","excess_mileage_fee":"","residual":{"integer_label":"","amount":""},"security_deposit":"","credit_qualification":"tier 1 approval (720+ beacon score)","down_payment":{"amount":"","zero_option":""},"document_fee":{"included":"","amount":""},"financing_lender":"Honda Financial Services"}},"transmission":"6-Speed Manual","special_id":"5073915248377856","trim":"LX","offer_type":{"Lease":{"tab_status":"false","term":"","amount":"","name":"Lease","tab_content_status":"false","position":"","status":"false","miles":""},"Low_Sale_Price":{"amount":"","tab_content_status":"false","name":"Low Sale Price","position":"","status":"false","tab_status":"false"},"Discount_From_MSRP":{"tab_status":"false","discount_type":"","amount":"","name":"Discount From MSRP","tab_content_status":"false","position":"","status":"false"},"APR":{"tab_status":"tab-visible","term":"36 Months","amount":"1.9","name":"APR","tab_content_status":"tab-content-visible","position":"","status":"true"},"Finance_Payment":{"position":"","status":"false","tab_status":"false","term":"","apr":"","amount":"","name":"Finance Payment","tab_content_status":"false"}},"stock_number":"H190182","make":"Honda","image_url":"https://storage.googleapis.com/vehicle_bucket/2018/honda/fit/0.png","checked_by_account_rep":"true","abbreviated_disclaimer":"*","similar_units":"6","abbreviated_disclaimer_for_graphic":"","matching_units":"3","inventory_file":"dealeron/10836.csv","special_order":"1","year":"2019"}


    echo "<hr>";

    $nextPageCursor = $entity->cursor();
}

// var_dump($specials);




?>