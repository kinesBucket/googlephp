# My API Key
```
YOUR_API_KEY=AIzaSyBy0yByxRfcu4P9hbkq70IyPtQWRGysKZY
```

## Download [Composer](https://getcomposer.org/download/) and [GMAIL API](https://developers.google.com/gmail/api/quickstart/php)
* open terminal, create a folder and type the following:
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
composer.phar require google/apiclient:^2.0
```
## Download credential.json form [Google Dev API dashboard](https://console.developers.google.com/apis/dashboard)
* Select **Credential** , in **OAuth 2.0 client IDs** section , download

## Get verification code from Rest URL

https://accounts.google.com/o/oauth2/auth?client_id=**{clientid}**&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=**{scope}**&response_type=code
[Scope List Here](https://developers.google.com/identity/protocols/googlescopes)
```
    https://accounts.google.com/o/oauth2/auth?client_id={clientid}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/gmail.readonly&response_type=code

    https://accounts.google.com/o/oauth2/auth?client_id={clientid}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/plus.business.manage&response_type=code

    https://accounts.google.com/o/oauth2/auth?client_id=192008506871-l55fd7jj52prjhnha5j5bv956ubuo2e4.apps.googleusercontent.com&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/analytics.readonly&response_type=code

    https://accounts.google.com/o/oauth2/auth?client_id=192008506871-l55fd7jj52prjhnha5j5bv956ubuo2e4.apps.googleusercontent.com&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/adwords&response_type=code

    https://accounts.google.com/o/oauth2/auth?client_id=604787890760-67fs5k50nlqdv48dta7b5ff39s6vc3qc.apps.googleusercontent.com&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/calendar.readonly&response_type=code

```

## To get token.json enter the given code to [getToken.html](/getToken.html)
```
or submit a post request to:
https://accounts.google.com/o/oauth2/token
code={code}&client_id={ClientId}&client_secret={ClientSecret}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&grant_type=authorization_code
```
## Make sure php.ini has Timezone set 
```
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
date.timezone = America/New_York
```

## others
* [filtering guide](https://developers.google.com/gmail/api/guides/filtering)
* [https://support.google.com/mail/answer/7190?hl=en](https://support.google.com/mail/answer/7190?hl=en)

## to run this example
* [http://localhost:8888/googlephp/get_emails_gmail.php](http://localhost:8888/googlephp/get_emails_gmail.php)
* [http://localhost:8888/googlephp/get_accounts.php](http://localhost:8888/googlephp/get_accounts.php)
* [http://localhost:8888/googlephp/DataStore.php](http://localhost:8888/googlephp/DataStore.php)
* [http://localhost:8888/googlephp/gcalendar.php](http://localhost:8888/googlephp/gcalendar.php)