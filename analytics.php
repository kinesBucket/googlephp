<?php

    // require str_replace("/api","/",__DIR__) . '/vendor/autoload.php';
    require __DIR__ . '/vendor/autoload.php';


    // Create and configure a new client object.
    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig('credendialNew.json');
    // $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
    $tokenPath = 'tokenAnalytics.json';    
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
        //  echo"token exists";
    } else {
         echo"token not exists";
    }
    $analytics = new Google_Service_Analytics($client);
    $accounts = $analytics->management_accounts->listManagementAccounts();
    var_dump($accounts);

?>