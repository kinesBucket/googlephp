<?php
// https://developers.google.com/calendar/quickstart/php
// https://console.developers.google.com/?authuser=0&project=quickstart-1558126232224
// https://developers.google.com/calendar/v3/reference/events/list
// http://localhost:8888/googlephp/getToken.html

// http://localhost:8888/googlephp/gcalendar.php



    require __DIR__ . '/vendor/autoload.php';

    function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
        $client->setAuthConfig('credentialsCalendar.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
    
        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = 'tokenCalendar.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
    
        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));
    
                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);
    
                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }    

    $client = getClient();
    $service = new Google_Service_Calendar($client);
    $c = date('c');

    $time = strtotime('05/21/2018');
    $old_date = date('c',$time);    

    // Print the next 10 events on the user's calendar.
    $calendarId = 'primary';
    $optParams = array(
      'maxResults' => 200,
      'orderBy' => 'startTime',
      'singleEvents' => true,
      'timeMin' => $old_date,
    );
    $results = $service->events->listEvents($calendarId, $optParams);
    $events = $results->getItems();
    

    if (empty($events)) {
        print "No upcoming events found.<br>";
    } else {
        print "<h1>Upcoming events:".$old_date."</h1><br>";
        foreach ($events as $event) {
            $start = $event->start->dateTime;
            if (empty($start)) {
                $start = $event->start->date;
            }
            $desc = $event->getSummary();

            
            $ignr = false;

            $pos = null;
            $excludeKeywords = array('Autopay','Bday','BDay','Birthday','Babath','Carpenter','Nat Grid');
            foreach ($excludeKeywords as $keyword) {
                $pos = (string) strpos($desc, $keyword) ;      
                if (null!=$pos) {
                    $ignr = true;
                    break;
                }                      
            }


            if (!$ignr) {
                echo $desc."  (".$start.")<br>";
                print $event->getLocation()."<br>";
                $id = $event->getId();
                print "<hr>";
            }
            // print "<br><br>";
        }
    }


?>