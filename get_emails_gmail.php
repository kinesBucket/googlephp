<?php

    // require str_replace("/api","/",__DIR__) . '/vendor/autoload.php';
    require __DIR__ . '/vendor/autoload.php';

    use PhpMimeMailParser\Parser;

    function decodeBody($body) {
        $rawData = $body;
        $sanitizedData = strtr($rawData,'-_', '+/');
        $decodedMessage = base64_decode($sanitizedData);
        if(!$decodedMessage){
            $decodedMessage = FALSE;
        }
        return $decodedMessage;
    }

    function getAttachment($part, $userId, $messageId, $service) {
        // echo 'filesize:'. $size = $part->getBody()->getSize( );    
        $filename = $part['filename'] ;                      
        $attachId = $part->getBody()->getAttachmentId();
        $size = $part->getBody()->getSize( );
        $partId = $part['partId'];
        $attachmentObj = $service->users_messages_attachments->get($userId, $messageId, $attachId);
        $data = $attachmentObj->getData(); //Get data from attachment object
        if ($data){
            $data = strtr($data, array('-' => '+', '_' => '/'));
            $imgdata=base64_decode($data);
            $dst = "./content/job/".$filename;
            $fp = fopen($dst, 'w');
            fputs($fp, $imgdata);
            fclose($fp); 
            echo "Attachment : <a href='".$dst."'>". $filename."</a>";              
        }
    }
   
    $client = new Google_Client();
    $client->setApplicationName('OOMDO');
    $client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
    $client->setAuthConfig('credendialNew.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
    // Load previously authorized token from a file, if it exists.
    $tokenPath = 'tokenNew.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    } else {
        echo"token not exists";
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            $authCode = "4/dQBx4SfMAV4fmLhYdHGpPpaxCm1eZ9D2TrrNViR2EewEy1t3LWA6UU0";//trim(fgets(STDIN));
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);
            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                echo "Ierr ";
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));  
    }
    $service = new Google_Service_Gmail($client);
    $userId = "venus@oomdo.com";
    
    $pageToken = NULL;
    //var_dump( $accessToken);
    $messages = array();
    $opt_param = array();
    // $opt_param['q'] = "is:unread";
    $opt_param['q'] = "after:2019/01/20";

    do {
      try {
        if ($pageToken) {
          $opt_param['pageToken'] = $pageToken;
        }
        $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
        if ($messagesResponse->getMessages()) {
          $messages = array_merge($messages, $messagesResponse->getMessages());
          $pageToken = $messagesResponse->getNextPageToken();
        }
      } catch (Exception $e) {
        echo 'An error occurred: ' . $e->getMessage();
      }
    } while ($pageToken);

    foreach ($messages as $message) {
        $msg = $service->users_messages->get($userId, $message->getId(), ['format' => 'full']);
        $payload = $msg->getPayload();
        $headers = $payload->getHeaders();

        $subject = "";
        $date = "";
        $from = "";
        $to = "";

        foreach ($headers as $header) {
            switch ($header["name"]) {
                case "Subject":
                    $subject = $header["value"];
                    break;
                case "From":
                    $from = $header["value"];
                    break;
                case "To":
                    $to = $header["value"];
                    break;
            }            
        }

        $body = $payload->getBody();
        $FOUND_BODY = decodeBody($body['data']);
        if(!$FOUND_BODY) {
            $parts = $payload->getParts();
            foreach ($parts  as $part) {
                if($part['body']) {
                    $FOUND_BODY = decodeBody($part['body']->data);
                    break;
                }
                // Last try: if we didn't find the body in the first parts, let's loop into the parts of the parts.
                if($part['parts'] && !$FOUND_BODY) {
                    foreach ($part['parts'] as $p) {
                        // replace 'text/html' by 'text/plain' if you prefer
                        if($p['mimeType'] === 'text/html' && $p['body']) {
                            $FOUND_BODY = decodeBody($p['body']->data);
                            break;
                        }
                    }
                }
                if($FOUND_BODY) {
                    break;
                }
            }
        }
        echo "<hr>";
        $parts = $payload->getParts();
        echo 'Message with ID: ' . $message->getId()." <br>subject:".$subject." <br>From:".$from." <br>To:".$to. '<br/>';
        print_r($FOUND_BODY);
        echo '<br/>';        

        foreach ($parts  as $part) {
            if (strlen($part['filename'])>0) {
                getAttachment($part, $userId, $message->getId(), $service);
            } else {
                foreach ($part['parts'] as $p) {
                    if (strlen($p['filename'])>0) {
                        getAttachment($p, $userId, $message->getId(), $service);
                    }
                }
            }
        }
    }    

?>