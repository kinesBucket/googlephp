<?php
    require __DIR__ . '/vendor/autoload.php';
    require_once __DIR__ .'/MyBusiness.php';

    $client = new Google_Client();
    $client->setApplicationName('OOMDO');                             
    $client->setScopes(['https://www.googleapis.com/auth/plus.business.manage']);    
    // $client->setAuthConfig('client_secret_galles.json');
    $client->setAuthConfig('credendialNew.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
    // $tokenPath = 'chevyToken.json';
    $tokenPath = 'tokenBusiness.json';    
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
        //  echo"token exists";
    } else {
         echo"token not exists";
    }
    $mybusinessService = new Google_Service_Mybusiness($client);

    // var_dump($mybusinessService->accounts);

    $accounts = $mybusinessService->accounts->listAccounts();
    // var_dump($accounts);
    foreach($accounts as $account) {
        echo $account['accountName']."<br>";
        $name = $account['name'];
        $locations = $mybusinessService->accounts_locations->listAccountsLocations($name)->getLocations();   
        foreach($locations as $location) {
            $locName = $location->getName();
            echo "location name : ".$locName."<br>";
            $address = $location->getAddress();              
            echo "location address : ".$address->getAddressLines()[0]. $address->getLocality(). $address->getAdministrativeArea(). $address->getPostalCode()."<br>";
            $reviews = $mybusinessService->accounts_locations_reviews;
            do{
                $listReviewsResponse = $reviews->listAccountsLocationsReviews($locName, array('pageSize' => 100,
                                    'pageToken' => $listReviewsResponse->nextPageToken));
            
                $reviewsList = $listReviewsResponse->getReviews();
                foreach ($reviewsList as $index => $review) {
                    var_dump($review);
                    /*Accesing $review Object
            
                    * $review->createTime;
                    * $review->updateTime;
                    * $review->starRating;
                    * $review->reviewer->displayName;
                    * $review->reviewReply->comment;
                    * $review->getReviewReply()->getComment();
                    * $review->getReviewReply()->getUpdateTime();
                    */            
                }            
            }while($listReviewsResponse->nextPageToken);
        }
    }
    
    // $error = error_get_last();
    // echo "error : ";$error['message'];    

?>    